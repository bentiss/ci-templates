include:
  - local: '/templates/ci-fairy.yml'

stages:
  - ci-fairy
  - sanity check
  - generic packages upload
  - bootstrapping
  - distributions
  - post children pipelines
  - deploy
  - publish
  - test published images


variables:
  FDO_CBUILD: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cbuild/{{ci_fairy.sha256sum('./bootstrap/cbuild')}}/cbuild

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: $CI_COMMIT_BRANCH

#
# Build the .fdo.ci-fairy base image
# we can do this now because this pipeline relies on a known
# working container-build-base image
#
ci-fairy images:
  stage: ci-fairy
  trigger:
    include:
      - local: '/.gitlab-ci/ci-fairy-ci.yml'
    strategy: depend

.ci-fairy-tag:
  variables:
    FDO_DISTRIBUTION_TAG: sha256-{{ci_fairy.hashfiles(*sha_sources)}}

.ci-fairy-local-image:
  extends:
    - .ci-fairy-tag
  image: $CI_REGISTRY_IMAGE/ci-fairy:$FDO_DISTRIBUTION_TAG


check merge request:
  extends:
    - .ci-fairy-local-image
  stage: sanity check
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=check-merge-request.xml
  artifacts:
    expire_in: 1 week
    when: on_failure
    paths:
      - check-merge-request.xml
    reports:
      junit: check-merge-request.xml
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
  # We allow this to fail because no MR may have been filed yet
  allow_failure: true


sanity check:
  extends: .ci-fairy-local-image
  stage: sanity check
  script:
    - make templates
    - git diff --exit-code && exit 0 || true

    - echo "some files were not generated through 'make templates' or
      have not been committed. Please edit the files under 'src', run
      'make templates' and then commit the result"
    - exit 1


check commits:
  extends: .ci-fairy-local-image
  stage: sanity check
  script:
    - ci-fairy check-commits --signed-off-by --junit-xml=results.xml
  except:
    - master@freedesktop/ci-templates
  variables:
    GIT_DEPTH: 100
    GIT_STRATEGY: clone
  artifacts:
    reports:
      junit: results.xml


pytest ci-fairy:
  extends: .ci-fairy-local-image
  stage: sanity check
  script:
    - pip3 install pytest
    - pytest -v --junitxml=results.xml
  artifacts:
    reports:
      junit: results.xml


black ci-fairy:
  extends: .ci-fairy-local-image
  stage: sanity check
  script:
    - pip3 install black
    - black --check tools


# Upload our current cbuild script to the project packages, the script is available through
# a link including its sha256 sum, the templates will curl this script at runtime.
upload cbuild:
  extends: .ci-fairy-local-image
  stage: generic packages upload
  script:
  - echo $FDO_CBUILD
  - |
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" -L $FDO_CBUILD -o cbuild

    # if the file is already in the registry, exit
  - diff cbuild bootstrap/cbuild > /dev/null && exit 0 || true
  - |
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
         --upload-file ./bootstrap/cbuild \
         $FDO_CBUILD | tee upload.txt

    # if the upload went fine, abort
  - grep '201 Created' upload.txt && exit 0 || true

  - |
    echo "I couldn't upload the cbuild script to the generic package
    registry.
    Please ensure that feature is enabled by going to:
      Settings -> General -> Visibility, project features, permissions
    and ensure 'Packages' is enabled" && exit 1


#
# Build the base images that the various distribution templates rely on
#
bootstrapping:
  stage: bootstrapping
  trigger:
    include:
      - local: '/.gitlab-ci/bootstrap-ci.yml'
    strategy: depend

#
# Test the templates for each distribution
#
{% for distribution in distribs %}
{{distribution}}:
  stage: distributions
  trigger:
    include:
      - local: '/.gitlab-ci/{{distribution}}-ci.yml'
    strategy: depend

{% endfor %}

#
# This is a dummy job to work around a gitlab bug.
# All distribution jobs above run in child pipelines. If a job in a child
# pipeline fails, that pipeline fails and our parent pipeline is stuck.
# Re-triggering the failed job does not continue the parent pipeline even when
# that job succeeds on the second attempt.
#
# The dummy job here checks for the results of the child pipelines via the API.
# If they are successful, this job succeeds and our pipeline continues.
#
# This job needs to be re-run manually after all child pipelines are fixed.
#
check children pipelines:
  extends: .ci-fairy-local-image
  stage: post children pipelines
  before_script:
    - apk add jq
  script:
    - URL=https://gitlab.freedesktop.org/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/bridges
    - DATA=$(curl $URL)
    - echo $DATA | jq
    - CHILD_STATUSES=$(echo $DATA | jq -r '.[] | .downstream_pipeline.status' | sort -u | grep -v null)
    - echo $CHILD_STATUSES
    - '[ "$CHILD_STATUSES" == "success" ]'
  when: always


pages:
  extends: .ci-fairy-local-image
  stage: deploy
  script:
  - apk add graphviz ttf-freefont
  - pip3 install sphinx sphinx-rtd-theme sphinxcontrib-autoyaml
  - sh -x doc/build-docs.sh
  - mv build public
  artifacts:
    paths:
    - public


#
# Template to publish to quay.io
# Requires: LOCAL_IMAGE_SPEC and DISTANT_IMAGE_SPEC to be set to the respective image paths
#
.publish.template:
  stage: publish
  image: $CI_REGISTRY_IMAGE/container-build-base:{{container_build_base_tag}}
  script:
    - skopeo login --username "$QUAY_USER" --password "$(cat $QUAY_TOKEN)" {{ci_templates_registry}}
    - set -x

    # transform the comma separated list into a space separated list
    - FDO_ARCHITECTURES=${FDO_ARCHITECTURES//,/ }

    # upload each individual layer if needed
    - |
      for arch in $FDO_ARCHITECTURES
      do
        export LOCAL_ARCH_IMAGE_SPEC="${LOCAL_IMAGE_SPEC//__arch__/$arch}"
        export LOCAL_IMAGE="$CI_REGISTRY_IMAGE/$LOCAL_ARCH_IMAGE_SPEC"
        export DISTANT_ARCH_IMAGE_SPEC="${DISTANT_IMAGE_SPEC//__arch__/$arch}"
        export DISTANT_IMAGE="{{ci_templates_registry}}{{ci_templates_registry_path}}:$DISTANT_ARCH_IMAGE_SPEC"

        # fetch the local and distant digest and layers to
        # ensure the image is not already on the remote

        # disable exit on failure
        set +e

        # we store the labels and the sha of the layers
        # we can not rely on the digest: the Name is different
        # but the labels are set with the commit ID and the pipeline/job
        # ids, which should ensure a correct match
        skopeo inspect docker://$LOCAL_IMAGE | jq '[.Labels, .Layers]' > local_sha

        # we store the same information from the distant registry
        skopeo inspect docker://$DISTANT_IMAGE | jq '[.Labels, .Layers]' > distant_sha

        # reenable exit on failure
        set -e

        # if the distant repo has an image, ensure we use the same
        # and finish the job if so
        diff distant_sha local_sha || touch .need_push

        # copy the original image into the distant registry
        if [[ -e .need_push ]]
        then
          rm .need_push
          skopeo copy docker://$LOCAL_IMAGE docker://$DISTANT_IMAGE
        fi
      done

    # build and push a manifest
    - |
      DISTANT_NOARCH_IMAGE_SPEC="${DISTANT_IMAGE_SPEC//__arch__}"
      DISTANT_IMAGE="{{ci_templates_registry_path}}:$DISTANT_NOARCH_IMAGE_SPEC"
      MANIFEST=${DISTANT_IMAGE/--/-}

      # remove leading '/'
      export MANIFEST=${MANIFEST/\//}

      buildah manifest create $MANIFEST

      for arch in $FDO_ARCHITECTURES
      do
        DISTANT_ARCH_IMAGE_SPEC="${DISTANT_IMAGE_SPEC//__arch__/$arch}"
        DISTANT_IMAGE="{{ci_templates_registry}}{{ci_templates_registry_path}}:$DISTANT_ARCH_IMAGE_SPEC"
        buildah manifest add $MANIFEST $DISTANT_IMAGE
      done

      # check if we already have this manifest in the registry
      buildah manifest inspect $MANIFEST > new_manifest.json
      buildah manifest inspect docker://{{ci_templates_registry}}/$MANIFEST > current_manifest.json || true

      diff -u current_manifest.json new_manifest.json || touch .need_push

      # and push it
      if [[ -e .need_push ]]
      then
        rm .need_push
        buildah manifest push --format v2s2 --all \
              $MANIFEST \
              docker://{{ci_templates_registry}}/$MANIFEST
      fi

    - set +x
    - skopeo logout {{ci_templates_registry}}


#
# Publish the various images to the remote registry
#
publish to {{ci_templates_registry}}:
  extends:
  - .publish.template
  parallel:
    matrix:
      - FDO_DISTRIBUTION_IMAGE:
          - container-build-base
        SUFFIX:
          - ''
        FDO_ARCHITECTURES:
          - x86_64,aarch64
        FDO_DISTRIBUTION_TAG:
          - '{{container_build_base_tag}}'
      - FDO_DISTRIBUTION_IMAGE:
          - qemu
        SUFFIX:
          - '-base'
        FDO_ARCHITECTURES:
          - x86_64
        FDO_DISTRIBUTION_TAG:
          - '{{qemu_tag}}'
  variables:
    # local format,   e.g. x86_64/mkosi-base-12324
    # distant format, e.g. mkosi-base-x86_64-12324
    LOCAL_IMAGE_SPEC:   __arch__/${FDO_DISTRIBUTION_IMAGE}${SUFFIX}:${FDO_DISTRIBUTION_TAG}
    DISTANT_IMAGE_SPEC: $FDO_DISTRIBUTION_IMAGE${SUFFIX}-__arch__-${FDO_DISTRIBUTION_TAG}
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'

#
# Publish our ci-fairy image on quay.io
#
publish ci-fairy to {{ci_templates_registry}}:
  extends:
    - .ci-fairy-tag
    - .publish.template
  variables:
    FDO_ARCHITECTURES:  x86_64
    LOCAL_IMAGE_SPEC:   ci-fairy:$FDO_DISTRIBUTION_TAG
    DISTANT_IMAGE_SPEC: ci-fairy-$FDO_DISTRIBUTION_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'

#
# Verify that all {{ci_templates_registry}} images directly referenced by our templates
# exist
#
test published images:
  image: $CI_REGISTRY_IMAGE/container-build-base:{{container_build_base_tag}}
  stage: test published images
  script:
    {% for remote_image in remote_images %}
    - skopeo inspect docker://{{remote_image}}
    {% endfor %}
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "freedesktop/ci-templates"'
